/**
 * @author Raluca Lehadus (aca10ril@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uk.ac.shef.dcs.com3504.query.QueryController;

/**
 * Servlet implementation class QueryServlet
 */
@WebServlet("/query")
public class QueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QueryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("QueryView.html").forward(request,
				response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String venue = request.getParameter("venue");
		String artist = request.getParameter("artist");
		String rdf = request.getParameter("rdf");
		
		QueryController qc = new QueryController(venue, artist, rdf);
		qc.execute();
		String json = qc.getJsonResult();
		
		PrintWriter out = response.getWriter();
		out.println(json);
	}
}
