/**
 * @author Mihai Popa-Matei (aca10map@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uk.ac.shef.dcs.com3504.extractor.Extractor;

/**
 * Servlet implementation class ExtractorServlet
 */
@WebServlet("/extractor")
public class ExtractorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExtractorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("ExtractorView.html").forward(request,
				response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Extractor extractor = new Extractor();
		extractor.start();
		String output = extractor.getResult();
		
		response.setContentType("application/force-download");
		response.setContentLength((int)output.length());
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Content-Disposition","attachment; filename=\"data.rdf\"");
		
		PrintWriter out = response.getWriter();
		out.println(output);
	}

}
