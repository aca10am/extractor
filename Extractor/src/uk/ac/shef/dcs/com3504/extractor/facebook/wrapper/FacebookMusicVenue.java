/**
 * @author Mihai Popa-Matei (aca10map@sheffield.ac.uk)
 */


package uk.ac.shef.dcs.com3504.extractor.facebook.wrapper;

import com.restfb.Facebook;
import com.restfb.json.JsonObject;

/**
 * Wrapping class for a music venue object, having the attributes extracted from
 * Facebook. It is needed to store the information extracted from Facebook, and
 * it is required by the restfb plugin.
 */
public class FacebookMusicVenue {
	/**
	 * Information about the location of the venue
	 */
	@Facebook
	JsonObject location;

	/**
	 * Address of the picture stored on facebook
	 */
	@Facebook
	String pic;

	/**
	 * Returns the attribute street
	 * 
	 * @return String
	 */
	public String getStreet() {
		return location.get("street").toString();
	}

	/**
	 * Returns the attribute city
	 * 
	 * @return String
	 */
	public String getCity() {
		return location.get("city").toString();
	}

	/**
	 * Returns the attribute country
	 * 
	 * @return String
	 */
	public String getCountry() {
		return location.get("country").toString();
	}

	/**
	 * Returns the attribute zip
	 * 
	 * @return String
	 */
	public String getZip() {
		return location.get("zip").toString();
	}

	/**
	 * Returns the attribute pic
	 * 
	 * @return String
	 */
	public String getPic() {
		return pic;
	}
}
