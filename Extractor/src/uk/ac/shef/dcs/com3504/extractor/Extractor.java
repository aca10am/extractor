/**
 * @author Andrei Misarca (aca10am@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.extractor;

import java.io.File;

import uk.ac.shef.dcs.com3504.mapping.EnvMapping;
import uk.ac.shef.dcs.com3504.mapping.OntMapping;
import uk.ac.shef.dcs.com3504.ontology.Ontology;

public class Extractor {

	private EnvMapping envMapping;
	private OntMapping ontMapping;
	private Ontology ontology;
	private String result;

	/**
	 * Retrieve the result of the extraction as a string.
	 * 
	 * @return String
	 */
	public String getResult() {
		return result;
	}

	/**
	 * The default constructor for the Extractor class, that creates a new
	 * Extractor object.
	 */
	public Extractor() {
		ontMapping = new OntMapping();
		envMapping = new EnvMapping();

		// Instantiate the ontology by passing the ontology files, the class
		// map, and the property map as parameters.
		File[] ontFiles = envMapping.getOntologyFiles();
		ontology = new Ontology(ontFiles, envMapping.getBaseURL());
	}

	/**
	 * Start the extractor, and store the result.
	 */
	public void start() {
		File root = envMapping.getSearchFolder();

		// Search for files with the given extension, and process them.
		String extension = envMapping.getFileExtension();
		FileSearcher.searchForFiles(root, extension, "", ontology, ontMapping,
				envMapping);

		// Store the result of the ontology.
		result = ontology.toString();
	}
}
