/**
 * @author Andrei Misarca (aca10am@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.extractor.facebook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import uk.ac.shef.dcs.com3504.mapping.FacebookMapping;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class URLMapping {

	/**
	 * The Map that will store pairs containing the relative link of the
	 * resource, and the Facebook ID.
	 */
	private static Map<String, String> urlMap = null;

	/**
	 * Get the Facebook Id for a relative link given.
	 * 
	 * @param internalURL
	 *            The relative link to a resource.
	 * @return String
	 */
	public static String getFacebookPageId(String internalURL) {
		if (urlMap == null) {
			intializeMapIfNull();
		}
		return urlMap.get(internalURL);
	}

	/**
	 * Initialize the Map, and parse the ontology given, in order to extract the
	 * information needed.
	 */
	private static void intializeMapIfNull() {
		urlMap = new HashMap<String, String>();

		// Create a default model, and load the ontology into memory.
		Model model = ModelFactory.createMemModelMaker().createDefaultModel();
		try {
			model.read(
					new FileInputStream(FacebookMapping
							.getConfigFile("ontology")), null);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		// Find the file where query for extracting the Facebook ID is located,
		// and read the whole file into a string.
		File facebookQueryFile = FacebookMapping.getConfigFile("queryFile");
		String queryString = null;
		try {
			queryString = new Scanner(facebookQueryFile).useDelimiter("\\Z")
					.next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		// Create a query from the content of the query file.
		ParameterizedSparqlString queryStr = new ParameterizedSparqlString(
				queryString);
		Query query = QueryFactory.create(queryStr.toString());
		QueryExecution qe = QueryExecutionFactory.create(query, model);

		// Execute the query and store the results.
		ResultSet results = qe.execSelect();

		while (results.hasNext()) {
			// Iterate through all the results, and get the current result.
			QuerySolution sol = results.next();

			// Get the internal URL for each individual, and remove the base
			// URL, so that only the relative path is stored. This way, the
			// mapping can be more extensible.
			String internalURL = sol.getLiteral("internalURL").getString();
			internalURL = internalURL.replace(
					FacebookMapping.getConfigResource("internalBaseURL"), "");

			// Get the Facebook Graph link, but keep only the Facebook Graph Id,
			// as only the id is needed to make requests.
			String facebookURL = sol.getLiteral("facebookURL").getString();
			facebookURL = facebookURL.replace(
					FacebookMapping.getConfigResource("facebookBaseURL"), "");

			urlMap.put(internalURL, facebookURL);
		}
	}
}
