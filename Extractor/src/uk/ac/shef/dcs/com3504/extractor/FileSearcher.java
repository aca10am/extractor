/**
 * @author Raluca Lehadus (aca10ril@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.extractor;

import java.io.File;

import uk.ac.shef.dcs.com3504.mapping.EnvMapping;
import uk.ac.shef.dcs.com3504.mapping.OntMapping;
import uk.ac.shef.dcs.com3504.ontology.Ontology;

public class FileSearcher {

	/**
	 * Search through all the files in the given folder, and subfolders, and if
	 * any file with the given suffix is found, then parse it and extract the
	 * information.
	 * 
	 * @param folder
	 *            The current folder where to search
	 * @param suffix
	 *            The suffix of the files that will be searched for
	 * @param url
	 *            The URL to the current folder
	 * @param ontology
	 *            The ontology used for creating the triples.
	 * @param ontMapping
	 *            The ontology mapping instance that keeps all the
	 *            configurations related to the ontology.
	 * @param envMapping
	 *            The environment mapping instance that keeps all the
	 *            configurations related to the environment.
	 */
	public static void searchForFiles(File folder, String suffix, String url,
			Ontology ontology, OntMapping ontMapping, EnvMapping envMapping) {
		// Get all the files and folder into the current folder.
		File[] subFolders = folder.listFiles();

		// Iterate through all files and folders, and if the current item is a
		// file, try to parse it and extract the information, and otherwise call
		// the function recursively on the current folder.
		for (File file : subFolders) {
			if (file.isDirectory()) {
				searchForFiles(file, suffix, url + "/" + file.getName(),
						ontology, ontMapping, envMapping);
			} else if (file.getName().endsWith(suffix)) {
				XMLParser xmlParser = new XMLParser(file, url, ontology,
						ontMapping, envMapping);
				xmlParser.parse();
			}
		}
	}
}
