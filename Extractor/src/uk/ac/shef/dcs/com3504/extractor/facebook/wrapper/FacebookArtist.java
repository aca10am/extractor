/**
 * @author Mihai Popa-Matei (aca10map@sheffield.ac.uk)
 */


package uk.ac.shef.dcs.com3504.extractor.facebook.wrapper;

import com.restfb.Facebook;

/**
 * Wrapping class for an artist object, having the attributes extracted from
 * Facebook. It is needed to store the information extracted from Facebook, and
 * it is required by the restfb plugin.
 */
public class FacebookArtist {
	/**
	 * Information about artist
	 */
	@Facebook
	String about;

	/**
	 * Number of likes on facebook
	 */
	@Facebook
	String fan_count;

	/**
	 * Picture address retrieved from facebook
	 */
	@Facebook
	String pic;

	/**
	 * Returns the attribute about
	 * 
	 * @return String
	 */
	public String getAbout() {
		return about;
	}

	/**
	 * Returns the attribute fan_count
	 * 
	 * @return String
	 */
	public String getFan_count() {
		return fan_count;
	}

	/**
	 * Returns the attribute pic
	 * 
	 * @return String
	 */
	public String getPic() {
		return pic;
	}
}
