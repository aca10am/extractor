/**
 * @author Raluca Lehadus (aca10ril@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.extractor.facebook;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.ac.shef.dcs.com3504.extractor.facebook.wrapper.FacebookMusicVenue;
import uk.ac.shef.dcs.com3504.mapping.FacebookMapping;
import uk.ac.shef.dcs.com3504.ontology.Ontology;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntProperty;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;

public class MusicVenueExtractor {
	/**
	 * The FacebookClient object that keeps the connection to the Facebook Graph
	 * API.
	 */
	private static FacebookClient facebookClient;

	/**
	 * Extract information from Facebook, and create RDF triples according to
	 * the given ontology.
	 * 
	 * @param individual
	 *            The individual objects whose properties will be retrieved from
	 *            Facebook.
	 * @param ontology
	 *            The ontology used for creating RDF triples.
	 * @param internalPath
	 *            The relative path to the individual.
	 * @param pageId
	 *            The Facebook Graph ID of the individual
	 */
	public static void extract(Individual individual, Ontology ontology,
			String internalPath, String pageId) {
		// Create a default Facebook Client, used for retrieving public
		// information.
		facebookClient = new DefaultFacebookClient();

		// Get the attributes of the current individual music venue.
		Map<String, String> musicVenueAttributes = getFacebookMusicVenue(pageId);

		// Get the list of properties retrieved from Facebook.
		List<String> musicVenueProperties = FacebookMapping.getProperties();

		for (String property : musicVenueProperties) {
			// For each property create a RDF triple if possible.
			String attribute = musicVenueAttributes.get(property);

			if (attribute != null) {
				OntProperty prop = ontology.getOntProperty(FacebookMapping
						.getProperty(property));

				if (prop.hasDomain(individual.getOntClass())) {
					individual.addProperty(prop, attribute);
				}
			}
		}
	}

	/**
	 * Returns from Facebook Graph API information about a Music Venue through a given page ID
	 * 
	 * @param pageId
	 * @return Map<String String>
	 */
	public static Map<String, String> getFacebookMusicVenue(String pageId) {
		// The FQL query used for retrieving information from Facebook Graph
		// API.
		String query = "SELECT location, pic FROM page WHERE page_id = "
				+ pageId;
		// Get the list of music venues returned by the query.
		List<FacebookMusicVenue> musicVenues = facebookClient.executeFqlQuery(
				query, FacebookMusicVenue.class);

		// Instantiate the map of attributes as an empty HashMap.
		Map<String, String> musicVenueAttributes = new HashMap<String, String>();

		// If there is at least one result, extract the information of the first
		// artist, and store them in the attributes map.
		if (!musicVenues.isEmpty()) {
			FacebookMusicVenue musicVenue = musicVenues.get(0);
			musicVenueAttributes.put("image", musicVenue.getPic());
			musicVenueAttributes.put("street", musicVenue.getStreet());
			musicVenueAttributes.put("city", musicVenue.getCity());
			musicVenueAttributes.put("country", musicVenue.getCountry());
			musicVenueAttributes.put("zip", musicVenue.getZip());
		}

		return musicVenueAttributes;
	}
}
