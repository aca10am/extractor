/**
 * @author Raluca Lehadus (aca10ril@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.extractor.dbpedia;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import uk.ac.shef.dcs.com3504.mapping.OntMapping;
import uk.ac.shef.dcs.com3504.ontology.Ontology;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Resource;

public class DBPediaExtractor {

	/**
	 * The DBPedia service end point, where the requests are sent.
	 */
	private final static String SERVICE_END_POINT = "http://dbpedia.org/sparql";

	/**
	 * Extract information from DBPedia, and create RDF triples to fill the
	 * given ontology.
	 * 
	 * @param individual
	 *            The individual to which the RDF triples will be attributed.
	 * @param individualUrl
	 *            The DBPedia link of the individual.
	 * @param ontMapping
	 *            The ontology mapping that contains information about the
	 *            ontology.
	 * @param ontology
	 *            The ontology used for creating RDF triples.
	 * 
	 */
	public static void extract(Individual individual, String individualUrl,
			OntMapping ontMapping, Ontology ontology) {

		// If the DBPedia link is given with a page path, then transform into a
		// link with the resource path.
		if (individualUrl.contains("page/")) {
			individualUrl = individualUrl.replace("page/", "resource/");
		}
		// Remove all character escaping signs.
		individualUrl = individualUrl.replace("\\", "");

		// Get all the DBPedia properties.
		List<String> dbPediaProperties = ontMapping.getDBPediaProperties();

		for (String propName : dbPediaProperties) {
			// Get the corresponding property in the ontology.
			OntProperty ontProp = ontology.getOntProperty(ontMapping
					.getDBPediaProperty(propName));

			// Query dbpedia only if the mapped property can be added to the
			// current individual.
			if (ontProp.hasDomain(individual.getOntClass())) {
				// Get the file where the DBPedia query is kept.
				File dbPediaQueryFile = ontMapping
						.getDBPediaQueryFile(propName);
				// Read the file in a single string.
				String queryString = null;
				try {
					queryString = new Scanner(dbPediaQueryFile).useDelimiter(
							"\\Z").next();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

				// Add the DBPedia resource link as being the individual about
				// which queries are done.
				ParameterizedSparqlString queryStr = new ParameterizedSparqlString(
						queryString);
				queryStr.setIri("indiv", individualUrl);

				// Create a SPARQL query for the DBPedia end point.
				Query query = QueryFactory.create(queryStr.toString());
				QueryExecution qe = QueryExecutionFactory.sparqlService(
						SERVICE_END_POINT, query);
				qe.setTimeout(1500);

				// Get the results.
				ResultSet results = qe.execSelect();

				while (results.hasNext()) {
					QuerySolution sol = results.next();

					// If a literal result is available, read it, and create an
					// RDF triples based on the result given.
					try {
						Literal propLiteral = sol.getLiteral(propName);
						createTripleFromLiteral(individual, ontProp,
								propLiteral);
						// If a literal result is not available, then read the
						// resource result, and create an RDF triples based on
						// the result given.
					} catch (ClassCastException e) {
						Resource propResource = sol.getResource(propName);
						createTripleFromResource(individual, ontProp,
								propResource);
					}
				}

				qe.close();
			}
		}

	}

	/**
	 * Create an RDF triple for the given individual, with the given property,
	 * having the given literal as value.
	 */
	private static void createTripleFromLiteral(Individual individual,
			OntProperty ontProp, Literal propLiteral) {
		if (propLiteral != null) {
			createTriple(individual, ontProp, propLiteral.getString());
		}
	}

	/**
	 * Create an RDF triple for the given individual, with the given property,
	 * having the given resource as value.
	 */
	private static void createTripleFromResource(Individual individual,
			OntProperty ontProp, Resource propResource) {
		if (propResource != null) {
			createTriple(individual, ontProp, propResource.getURI());
		}
	}

	/**
	 * Generate an RDF triple containing a data property.
	 */
	private static void createTriple(Individual individual, OntProperty prop,
			String content) {
		individual.addProperty(prop, content);
	}

}
