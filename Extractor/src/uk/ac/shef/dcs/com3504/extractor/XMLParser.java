/**
 * @author Andrei Misarca (aca10am@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.extractor;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.ac.shef.dcs.com3504.extractor.dbpedia.DBPediaExtractor;
import uk.ac.shef.dcs.com3504.extractor.facebook.ArtistExtractor;
import uk.ac.shef.dcs.com3504.extractor.facebook.MusicVenueExtractor;
import uk.ac.shef.dcs.com3504.extractor.facebook.URLMapping;
import uk.ac.shef.dcs.com3504.mapping.EnvMapping;
import uk.ac.shef.dcs.com3504.mapping.FacebookMapping;
import uk.ac.shef.dcs.com3504.mapping.OntMapping;
import uk.ac.shef.dcs.com3504.ontology.Ontology;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;

public class XMLParser {
	/**
	 * The ontology used for creating RDF triples.
	 */
	private Ontology ontology;
	/**
	 * The ontology mapping instance that keeps all the configurations related
	 * to the ontology.
	 */
	private OntMapping ontMapping;
	/**
	 * The document created based on the current file that needs to be parsed.
	 */
	private Document document;
	/**
	 * The full URL to the current file.
	 */
	private String url;
	/**
	 * The baseURL of the website
	 */
	private String baseURL;
	/**
	 * The relative path to the current file.
	 */
	private String internalPath;
	/**
	 * The XML tag where the class is defined.
	 */
	private String classTag;

	/**
	 * Create a new XMLParser object that can parse an XML file, and create the
	 * RDF triples.
	 * 
	 * @param file
	 *            The current file that will be parsed
	 * @param internalPath
	 *            The relative path to the current file
	 * @param ontology
	 *            The ontology used for creating the triples
	 * @param ontMapping
	 *            The ontology mapping instance that keeps all the
	 *            configurations related to the ontology.
	 * @param envMapping
	 *            The environment mapping instance that keeps all the
	 *            configurations related to the environment.
	 */
	public XMLParser(File file, String internalPath, Ontology ontology,
			OntMapping ontMapping, EnvMapping envMapping) {
		this.ontology = ontology;
		document = generateDocument(file);
		baseURL = envMapping.getBaseURL();
		this.url = baseURL + internalPath;
		this.internalPath = internalPath;
		this.ontMapping = ontMapping;
		classTag = envMapping.getClassTag();
	}

	/**
	 * Generate a Document object given a file
	 */
	private Document generateDocument(File file) {
		// Create a DocumentBuilderFactory
		DocumentBuilderFactory domFactory = DocumentBuilderFactory
				.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder = null;
		Document doc = null;

		// Generate a DocumentBuilder instance
		try {
			builder = domFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		// Generate a Document
		try {
			doc = builder.parse(file);
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}

	/**
	 * Evaluate a given XPath and return the result as a NodeList object.
	 */
	private NodeList evaluateXPath(String xPath) {
		// Create a new XPath instance.
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		XPathExpression expr;

		// Evaluate the XPath, and return a NodeList, each element of the
		// NodeList is a result of the XPath query.
		NodeList result = null;
		try {
			expr = xpath.compile(xPath);
			result = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Get the name of the class, as it is stored in the class tag defined in
	 * the properties. If the class name cannot be retrieved, return null.
	 */
	private String getFileClass() {
		NodeList result = evaluateXPath("//" + classTag);

		// If there are more than one result, return the content of the first
		// item.
		if (result.getLength() > 0) {
			return result.item(0).getTextContent().toString();
		} else {
			return null;
		}
	}

	/**
	 * Parse the content of the file, searching for the specified tags in the
	 * properties mapping file. Afterwards, create the RDF triples containing
	 * the extracted information. If possible, extract information from Facebook
	 * and DBPedia as well, and create RDF triples containing the extracted
	 * information.
	 */
	public void parse() {
		// If the document is null, then the file cannot be parsed, so do not
		// continue.
		if (document == null) {
			return;
		}

		// If the class name cannot be retrieved, RDF triples cannot be created,
		// so do not continue parsing.
		String className = getFileClass();
		String ontClassName = ontMapping.getOntClassName(className);
		if (className == null || ontClassName == null) {
			return;
		}

		// Get the ontology class corresponding to the extracted one, and create
		// an instance of that class.
		OntClass domainClass = ontology.getOntClass(ontClassName);
		Individual individual = domainClass.createIndividual(url);

		// Iterate through all possible properties, and create triples from that
		// tags, and the information contained in those tags.
		List<String> propertyTags = ontMapping.getPropertyTags();
		for (String tag : propertyTags) {
			createTripleFromTag(tag, individual);
		}

		// Extract information from DBPedia as well, if possible.
		extractFromDbpedia(individual);

		// If information can be extracted from Facebook, then extract it.
		if (className.equals(FacebookMapping.getConfigResource("artistClass"))) {
			extractArtistFromFacebook(individual);
		}

		// If information can be extracted from Facebook, then extract it.
		if (className.equals(FacebookMapping
				.getConfigResource("musicVenueClass"))) {
			extractMusicVenueFromFacebook(individual);
		}
	}

	/**
	 * Extract information about an artist from Facebook, and then create RDF
	 * triples to represent that information in the ontology.
	 * 
	 * @param individual
	 *            The instance about which the triples will be created.
	 */
	private void extractArtistFromFacebook(Individual individual) {
		// Find the Facebook Open Graph page id.
		String pageId = URLMapping.getFacebookPageId(internalPath);

		// If the page id is not null, then extract the information from
		// Facebook.
		if (pageId != null) {
			ArtistExtractor.extract(individual, ontology, internalPath, pageId);
		}
	}

	/**
	 * Extract information about a music venue from Facebook, and then create
	 * RDF triples to represent that information in the ontology.
	 * 
	 * @param individual
	 *            The instance about which the triples will be created.
	 */
	private void extractMusicVenueFromFacebook(Individual individual) {
		// Find the Facebook Open Graph page id.
		String pageId = URLMapping.getFacebookPageId(internalPath);

		// If the page id is not null, then extract the information from
		// Facebook.
		if (pageId != null) {
			MusicVenueExtractor.extract(individual, ontology, internalPath,
					pageId);
		}
	}

	/**
	 * Extract information from DBPedia, if the DBPedia tag is present, and then
	 * create RDF triples containing that information to fill the ontology.
	 * 
	 * @param individual
	 *            The instance about which the triples will be created.
	 */
	private void extractFromDbpedia(Individual individual) {
		// Search for the DBPedia tag in the XML file.
		String dbpediaTag = ontMapping.getDBPediaTag();
		NodeList result = evaluateXPath("//" + dbpediaTag);

		// If the DBPedia tag has been found, then extract information from that
		// specific link.
		if (result.getLength() > 0) {
			String individualURL = result.item(0).getTextContent().toString();
			DBPediaExtractor.extract(individual, individualURL, ontMapping,
					ontology);
		}
	}

	/**
	 * Search if the given tag can be found in the XML file, and if it can be
	 * found, create a triple with the given individual, the property associated
	 * with the tag, and the value extracted from the XML file.
	 */
	private void createTripleFromTag(String tag, Individual individual) {
		// Check if there is any URL attribute. If there is one, it means
		// that there is an object property, and create an object property,
		// rather than a data property.
		NodeList result = evaluateXPath("//" + tag + "/@url");
		if (result.getLength() > 0) {
			createObjectTripleFromXPathNodeList(tag, result, individual);
		} else {
			// Get the value of the nodes, in order to be able to create RDF
			// triples having data properties.
			result = evaluateXPath("//" + tag);

			createDataTripleFromXPathNodeList(tag, result, individual);
		}
	}

	/**
	 * Create all the object triples given the tag in XML file, the XPath result
	 * of the query in the XML file, and the instance of the class defined in
	 * the XML file.
	 */
	private void createObjectTripleFromXPathNodeList(String tag,
			NodeList result, Individual individual) {
		// Iterate through all the properties in order to create the RDF
		// triples.
		for (int i = 0; i < result.getLength(); i++) {
			// Compute the URL of the object instance.
			String objectUrl = baseURL
					+ result.item(i).getTextContent().toString();
			// Get the name of the properties that match the current
			// tag, and iterate over them.
			String[] ontPropertiesName = ontMapping.getOntPropertyName(tag);

			for (String ontPropertyName : ontPropertiesName) {
				// Get the OntProperty model from the ontology.
				OntProperty prop = ontology.getOntProperty(ontPropertyName);

				// Create the RDF triple only the property can be added
				// to the current class.
				if (prop.hasDomain(individual.getOntClass())) {
					createObjectPropertyTriple(individual, prop, objectUrl);
				}
			}
		}
	}

	/**
	 * Create all the data triples given the tag in XML file, the XPath result
	 * of the query in the XML file, and the instance of the class defined in
	 * the XML file.
	 */
	private void createDataTripleFromXPathNodeList(String tag, NodeList result,
			Individual individual) {
		// Iterate through the results and create RDF triples having
		// data properties.
		for (int i = 0; i < result.getLength(); i++) {
			// Get the value of the node.
			String content = result.item(i).getTextContent().toString();

			// Get the name of the properties that match the current
			// tag, and iterate over them.
			String[] ontPropertiesName = ontMapping.getOntPropertyName(tag);

			for (String ontPropertyName : ontPropertiesName) {
				// Get the OntProperty model from the ontology.
				OntProperty prop = ontology.getOntProperty(ontPropertyName);

				// Create the RDF triple only the property can be added
				// to the current class.
				if (prop.hasDomain(individual.getOntClass())) {
					createDataPropertyTriple(individual, prop, content);
				}
			}
		}
	}

	/**
	 * Generate an RDF triple containing a data property.
	 */
	private void createDataPropertyTriple(Individual individual,
			OntProperty prop, String content) {
		individual.addProperty(prop, content.replaceAll("[^\\x20-\\x7e]", ""));
	}

	/**
	 * Generate an RDF triple containing an object property.
	 */
	private void createObjectPropertyTriple(Individual individual,
			OntProperty prop, String objectUrl) {
		OntClass rangeClass = prop.getRange().asClass();
		Individual objectIndividual = rangeClass.createIndividual(objectUrl);
		individual.addProperty(prop, objectIndividual);
	}
}
