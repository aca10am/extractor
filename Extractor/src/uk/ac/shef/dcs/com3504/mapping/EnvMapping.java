/**
 * @author Andrei Misarca (aca10am@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.mapping;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class EnvMapping extends Mapping {

	/**
	 * The file where the environment properties are stored.
	 */
	private final String ENV_PROPERTIES_FILE = "environment";

	/**
	 * The actual environment properties map.
	 */
	private Map<String, String> envPropertiesMap;

	/**
	 * The public constructor that reads the file, and puts all the pairs into
	 * the map.
	 */
	public EnvMapping() {
		ResourceBundle resourceBundle = ResourceBundle
				.getBundle(ENV_PROPERTIES_FILE);
		envPropertiesMap = resourceBundleToMap(resourceBundle);
	}

	/**
	 * Return the ontology files defined by the properties file.
	 * 
	 * @return File[]
	 */
	public File[] getOntologyFiles() {
		// Get all the ontology file names.
		String[] fileNames = envPropertiesMap.get("ontologyFiles").split(",");
		List<File> files = new ArrayList<File>();

		// For each file name, add to the list of files, the file corresponding
		// to that name.
		for (String fileName : fileNames) {
			files.add(getFile(fileName));
		}

		// Transform the List<File> to File[] and return it.
		return files.toArray(new File[0]);
	}

	/**
	 * Get the search folder.
	 * 
	 * @return String
	 */
	public File getSearchFolder() {
		return getFile(envPropertiesMap.get("searchFolder"));
	}

	/**
	 * Get the base URL for the current working website.
	 * 
	 * @return String
	 */
	public String getBaseURL() {
		return envPropertiesMap.get("baseURL");
	}

	/**
	 * Get the extension of the files that contain data.
	 * 
	 * @return String
	 */
	public String getFileExtension() {
		return envPropertiesMap.get("fileExtension");
	}

	/**
	 * Get the tag that defines the class in the files that contain data.
	 * 
	 * @return String
	 */
	public String getClassTag() {
		return envPropertiesMap.get("classTag");
	}

	/**
	 * Get the file that contains the query for retrieving attributes of the
	 * artists.
	 * 
	 * @return File
	 */
	public File getArtistAttributesQueryFile() {
		return getFile(envPropertiesMap.get("artistAttributesQuery"));
	}

	/**
	 * Get the file that contains the query for retrieving the tracks of the
	 * albums.
	 * 
	 * @return File
	 */
	public File getAlbumTracksQueryFile() {
		return getFile(envPropertiesMap.get("albumTracksQuery"));
	}

	/**
	 * Get the file that contains the query for retrieving attributes of the
	 * music venues.
	 * 
	 * @return File
	 */
	public File getMusicVenueAttributesQueryFile() {
		return getFile(envPropertiesMap.get("musicVenueAttributesQuery"));
	}
}
