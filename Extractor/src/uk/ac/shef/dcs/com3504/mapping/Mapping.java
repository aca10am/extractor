/**
 * @author Raluca Lehadus (aca10ril@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.mapping;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import uk.ac.shef.dcs.com3504.extractor.Extractor;

public class Mapping {

	/**
	 * Transform a resource bundle into a HashMap<String, String>.
	 * 
	 * @param resourceBundle
	 * @return HashMap<String, String>
	 */
	protected Map<String, String> resourceBundleToMap(
			ResourceBundle resourceBundle) {
		Map<String, String> map = new HashMap<String, String>();

		// Iterate over the keys of the resource bundle, and add entries in the
		// map.
		Enumeration<String> keys = resourceBundle.getKeys();
		while (keys.hasMoreElements()) {
			// Get the key
			String key = keys.nextElement();
			// Get the value
			String value = resourceBundle.getString(key);
			map.put(key, value);
		}

		return map;
	}
	
	/**
	 * Returns a file by specifying an address
	 * 
	 * @param url
	 * @return File
	 */
	protected File urlToFile(URL url) {
		File file = null;
		try {
			file = new File(url.toURI());
		} catch (URISyntaxException e) {
			file = new File(url.getPath());
		}
		return file;
	}

	/**
	 * Returns a file by inputing a name of a file
	 * 
	 * @param fileName
	 * @return
	 */
	protected File getFile(String fileName) {
		URL url = Extractor.class.getClassLoader().getResource(fileName);
		return urlToFile(url);
	}
}
