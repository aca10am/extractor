/**
 * @author Raluca Lehadus (aca10ril@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.mapping;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class OntMapping extends Mapping {
	private final String CLASS_MAPPING_FILE = "classMapping";
	private final String PROPERTY_MAPPING_FILE = "propertyMapping";
	private final String DBPEDIA_CONFIG_MAPPING_FILE = "dbPediaConfigMapping";
	private final String DBPEDIA_PROPERTIES_MAPPING_FILE = "dbPediaPropertiesMapping";

	private Map<String, String> classMap;
	private Map<String, String> propertyMap;
	private Map<String, String> dbpediaConfigMap;
	private Map<String, String> dbpediaPropertiesMap;

	public OntMapping() {
		ResourceBundle resourceBundle = ResourceBundle
				.getBundle(CLASS_MAPPING_FILE);
		classMap = resourceBundleToMap(resourceBundle);

		resourceBundle = ResourceBundle.getBundle(PROPERTY_MAPPING_FILE);
		propertyMap = resourceBundleToMap(resourceBundle);

		resourceBundle = ResourceBundle.getBundle(DBPEDIA_CONFIG_MAPPING_FILE);
		dbpediaConfigMap = resourceBundleToMap(resourceBundle);

		resourceBundle = ResourceBundle
				.getBundle(DBPEDIA_PROPERTIES_MAPPING_FILE);
		dbpediaPropertiesMap = resourceBundleToMap(resourceBundle);
	}

	/**
	 * Return the DBPedia tag as a string
	 * 
	 * @return String
	 */
	public String getDBPediaTag() {
		return dbpediaConfigMap.get("tag");
	}

	/**
	 * Return a file for making queries 
	 * 
	 * @param fileName
	 * @return String
	 */
	public File getDBPediaQueryFile(String fileName) {
		String filePath = dbpediaConfigMap.get("queryFolder") + "/" + fileName
				+ ".rq";
		return getFile(filePath);
	}

	/**
	 * Return a property from DBPedia 
	 * 
	 * @param property
	 * @return String
	 */
	public String getDBPediaProperty(String property) {
		return dbpediaPropertiesMap.get(property);
	}

	/**
	 * Return a list of DBPedia properties
	 * 
	 * @return List<String>
	 */
	public List<String> getDBPediaProperties() {
		return new ArrayList<String>(dbpediaPropertiesMap.keySet());
	}

	/**
	 * Return a list of Strings representing property tags
	 * 
	 * @return List<String>
	 */
	public List<String> getPropertyTags() {
		return new ArrayList<String>(propertyMap.keySet());
	}

	/**
	 * Return an array of ontologies based on a tag name
	 * 
	 * @param tagName
	 * @return String[]
	 */
	public String[] getOntPropertyName(String tagName) {
		return propertyMap.get(tagName).split(",");
	}

	/**
	 * Return an ontology based on a class name
	 * 
	 * @param className
	 * @return
	 */
	public String getOntClassName(String className) {
		return classMap.get(className);
	}
}
