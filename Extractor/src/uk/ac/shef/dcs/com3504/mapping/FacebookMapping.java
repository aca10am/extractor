/**
 * @author Andrei Misarca (aca10am@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.mapping;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class FacebookMapping {

	/**
	 * The name of the file that contains the configurations regarding Facebook.
	 */
	private final static String FACEBOOK_CONFIG_FILE = "facebookConfigMapping";

	/**
	 * The name of the file that contains the properties that will be retrieved
	 * from Facebook.
	 */
	private final static String FACEBOOK_PROPERTY_FILE = "facebookPropertyMapping";

	/**
	 * The map containing the configurations for Facebook.
	 */
	private static Map<String, String> facebookConfigMap = null;

	/**
	 * The map containing the properties for Facebook.
	 */
	private static Map<String, String> facebookPropertyMap = null;

	/**
	 * Instantiate the Facebook config map, and populate it with the pairs in
	 * the configuration file.
	 */
	private static void initializeFacebookConfigMap() {
		if (facebookConfigMap == null) {
			ResourceBundle resourceBundle = ResourceBundle
					.getBundle(FACEBOOK_CONFIG_FILE);
			facebookConfigMap = resourceBundleToMap(resourceBundle);
		}
	}

	/**
	 * Get a configuration resource related to Facebook.
	 * 
	 * @param config
	 *            The name of the attribute required.
	 * @return String
	 */
	public static String getConfigResource(String config) {
		initializeFacebookConfigMap();
		return facebookConfigMap.get(config);
	}

	/**
	 * Get a configuration file related to Facebook.
	 * 
	 * @param config
	 *            The name of the file required.
	 * @return File
	 */
	public static File getConfigFile(String fileName) {
		initializeFacebookConfigMap();
		return stringToFile(facebookConfigMap.get(fileName));
	}

	/**
	 * Instantiate the Facebook properties map, and populate it with the pairs
	 * in the properties file.
	 */
	private static void initializeFacebookPropertyMap() {
		if (facebookPropertyMap == null) {
			ResourceBundle resourceBundle = ResourceBundle
					.getBundle(FACEBOOK_PROPERTY_FILE);
			facebookPropertyMap = resourceBundleToMap(resourceBundle);
		}
	}

	/**
	 * Get a property present in the Facebook properties file.
	 * 
	 * @param config
	 *            The name of the attribute required.
	 * @return String
	 */
	public static String getProperty(String property) {
		initializeFacebookPropertyMap();
		return facebookPropertyMap.get(property);
	}

	/**
	 * Get all the properties present in the Facebook properties file.
	 * 
	 * @return List<String>
	 */
	public static List<String> getProperties() {
		initializeFacebookPropertyMap();
		return new ArrayList<String>(facebookPropertyMap.keySet());
	}

	/**
	 * Return the file located at the given URL.
	 * 
	 * @param url
	 * @return File
	 */
	private static File urlToFile(URL url) {
		File file = null;
		// First try to get the URL without escaping it.
		try {
			file = new File(url.toURI());
			// If escaping needed, then escape it.
		} catch (URISyntaxException e) {
			file = new File(url.getPath());
		}
		return file;
	}

	/**
	 * Return the File that has the given name.
	 * 
	 * @param fileName
	 *            The name of the file that must be retrieved.
	 * @return File
	 */
	private static File stringToFile(String fileName) {
		// First, get the URL of the file.
		URL url = FacebookMapping.class.getClassLoader().getResource(fileName);

		// Return the file located at the extracted URL.
		return urlToFile(url);
	}

	/**
	 * Transform a resource bundle into a HashMap<String, String>.
	 * 
	 * @param resourceBundle
	 * @return HashMap<String, String>
	 */
	private static Map<String, String> resourceBundleToMap(
			ResourceBundle resourceBundle) {
		Map<String, String> map = new HashMap<String, String>();

		// Iterate over the keys of the resource bundle, and add entries in the
		// map.
		Enumeration<String> keys = resourceBundle.getKeys();
		while (keys.hasMoreElements()) {
			// Get the key
			String key = keys.nextElement();
			// Get the value
			String value = resourceBundle.getString(key);
			map.put(key, value);
		}

		return map;
	}
}
