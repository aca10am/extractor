/**
 * @author Andrei Misarca (aca10am@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.ontology;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class Ontology {
	/**
	 * The ontology model corresponding to the ontology.
	 */
	private OntModel ontModel;
	/**
	 * The domain URL for the ontology.
	 */
	private String baseURL;

	/**
	 * Retrieve the ontology model
	 * 
	 * @return OntModel
	 */
	public OntModel getModel() {
		return ontModel;
	}

	public Ontology(File[] ontFiles, String baseURL) {
		// Instantiate the ontology model, and load all the ontology files.
		ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		this.baseURL = baseURL;
		loadOntologies(ontFiles);
	}

	/**
	 * Return the ontology class from the model, having the name the same as the
	 * parameter.
	 * 
	 * @param ontClassString
	 * @return OntClass
	 */
	public OntClass getOntClass(String ontClassString) {
		return ontModel.getOntClass(baseURL + ontClassString);
	}

	/**
	 * Return the ontology property from the model, having the name the same as
	 * the parameter.
	 * 
	 * @param ontPropertyString
	 * @return OntProperty
	 */
	public OntProperty getOntProperty(String ontPropertyString) {
		return ontModel.getOntProperty(baseURL + ontPropertyString);
	}

	/**
	 * Read all the ontologies into the previously created OntModel instance.
	 * 
	 * @param ontFiles
	 *            The files containing the necessarily ontologies.
	 */
	private void loadOntologies(File[] ontFiles) {
		for (File file : ontFiles) {
			try {
				// Iterate through all the ontologies, and read each file into
				// memory.
				ontModel.read(new FileInputStream(file), baseURL);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Return the stored triples as a String.
	 * 
	 * @return String
	 */
	public String toString() {
		// Create a ByteArrayOutputStream to store the content of ontModel.
		ByteArrayOutputStream ba = new ByteArrayOutputStream();
		
		// Write the content into the output stream, and return it as a String.
		ontModel.write(ba);
		return ba.toString();
	}
}
