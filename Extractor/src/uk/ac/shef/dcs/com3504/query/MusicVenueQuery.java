/**
 * @author Mihai Popa-Matei (aca10map@sheffield.ac.uk)
 */


package uk.ac.shef.dcs.com3504.query;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

import uk.ac.shef.dcs.com3504.mapping.EnvMapping;
import uk.ac.shef.dcs.com3504.query.wrapper.MusicVenue;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class MusicVenueQuery {

	/**
	 * Attribute to store the rdf model inputted
	 */
	private Model model;
	
	/**
	 * String to store the artist name
	 */
	private String musicVenueName;
	
	/**
	 * Attribute to store the properties describing the environment of the query
	 */
	private EnvMapping envMapping;

	/**
	 * Constructor used to create a new model in which to read the rdf input,
	 * set the music venue name and store the environmental mapping
	 * 
	 * @param musicVenue
	 *            The music venue to be inspected
	 * @param rdf
	 *            Input the rdf file to be read   
	 * @param envMapping
	 *            Attribute to store the mapping needed 
	 *            for querying the Artist property          
	 */
	public MusicVenueQuery(String musicVenue, InputStream rdf,
			EnvMapping envMapping) {
		this.envMapping = envMapping;
		musicVenueName = musicVenue;
		model = ModelFactory.createMemModelMaker().createDefaultModel();
		model.read(rdf, null);
	}

	/**
	 * Function used to process and store all available attributes 
	 * for the music venue name inputed         
	 */
	public MusicVenue execute() {
		MusicVenue musicVenue = new MusicVenue(musicVenueName);

		// Store the attributes available regarding musicVenues
		File musicVenueAttributesQueryFile = envMapping
				.getMusicVenueAttributesQueryFile();
		String queryString = null;
		
		// Read each attribute available in file
		try {
			queryString = new Scanner(musicVenueAttributesQueryFile)
					.useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		// Create SPARQL query string to set the name of the artist
		ParameterizedSparqlString queryStr = new ParameterizedSparqlString(
				queryString);
		queryStr.setLiteral("name", musicVenueName);

		Query query = QueryFactory.create(queryStr.toString());
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet results = qe.execSelect();

		// Perform check of whether there are more queries to be performed
		if (results.hasNext()) {
			musicVenue.setFound(true);
		} else {
			musicVenue.setFound(false);
		}

		// perform checks of what attributes are available and store them
		while (results.hasNext()) {
			QuerySolution sol = results.next();
			if (sol.getLiteral("image") != null) {
				musicVenue.setImage(sol.getLiteral("image").toString());
			}
			if (sol.getLiteral("wikiPage") != null) {
				musicVenue.setWikiPage(sol.getLiteral("wikiPage").toString());
			}
			if (sol.getLiteral("description") != null) {
				musicVenue.setDescription(sol.getLiteral("description")
						.toString());
			}
			if (sol.getLiteral("geoLat") != null) {
				musicVenue.setGeoLat(sol.getLiteral("geoLat").toString());
			}
			if (sol.getLiteral("geoLon") != null) {
				musicVenue.setGeoLon(sol.getLiteral("geoLon").toString());
			}
			if (sol.getLiteral("street") != null) {
				musicVenue.setStreet(sol.getLiteral("street").toString());
			}
			if (sol.getLiteral("city") != null) {
				musicVenue.setCity(sol.getLiteral("city").toString());
			}
			if (sol.getLiteral("country") != null) {
				musicVenue.setCountry(sol.getLiteral("country").toString());
			}
			if (sol.getLiteral("zip") != null) {
				musicVenue.setZip(sol.getLiteral("zip").toString());
			}
		}

		qe.close();

		return musicVenue;
	}
}
