/**
 * @author Andrei Misarca (aca10am@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.query;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

import uk.ac.shef.dcs.com3504.mapping.EnvMapping;
import uk.ac.shef.dcs.com3504.query.wrapper.Album;
import uk.ac.shef.dcs.com3504.query.wrapper.Artist;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Class to perform queries on the artist property
 */
public class ArtistQuery {
	/**
	 * Attribute to store the rdf model inputted
	 */
	private Model model;

	/**
	 * String to store the artist name
	 */
	private String artistName;

	/**
	 * Attribute to store the properties describing the environment of the query
	 */
	private EnvMapping envMapping;

	/**
	 * Constructor used to create a new model in which to read the rdf input,
	 * set the artist name and store the environmental mapping
	 * 
	 * @param artist
	 *            The artist to be inspected
	 * @param rdf
	 *            Input the rdf file to be read
	 * @param envMapping
	 *            Attribute to store the mapping needed for querying the Artist
	 *            property
	 */
	public ArtistQuery(String artist, InputStream rdf, EnvMapping envMapping) {
		this.envMapping = envMapping;
		artistName = artist;
		model = ModelFactory.createMemModelMaker().createDefaultModel();
		model.read(rdf, null);
	}

	/**
	 * Function used to process and store all available attributes for the
	 * artist name inputed
	 */
	public Artist execute() {
		Artist artist = new Artist(artistName);

		// Store the attributes available regarding Artists
		File artistAttributesQueryFile = envMapping
				.getArtistAttributesQueryFile();
		String queryString = null;

		// Read each attribute available in file
		try {
			queryString = new Scanner(artistAttributesQueryFile).useDelimiter(
					"\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		// Create SPARQL query string to set the name of the artist
		ParameterizedSparqlString queryStr = new ParameterizedSparqlString(
				queryString);
		queryStr.setLiteral("name", artistName);

		Query query = QueryFactory.create(queryStr.toString());
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet results = qe.execSelect();

		// Perform check of whether there are more queries to be performed
		if (results.hasNext()) {
			artist.setFound(true);
		} else {
			artist.setFound(false);
		}

		// perform checks of what attributes are available and store them
		while (results.hasNext()) {
			QuerySolution sol = results.next();
			if (sol.getLiteral("biography") != null) {
				artist.setBiography(sol.getLiteral("biography").toString());
			}
			if (sol.getLiteral("image") != null) {
				artist.setImage(sol.getLiteral("image").toString());
			}
			if (sol.getLiteral("wikiPage") != null) {
				artist.setWikiPage(sol.getLiteral("wikiPage").toString());
			}
			if (sol.getResource("album") != null) {
				artist.addAlbum(createAlbum(sol.getResource("album")));
			}
			if (sol.getLiteral("likes") != null) {
				artist.setLikes(Integer.parseInt(sol.getLiteral("likes")
						.toString()));
			}
			if (sol.getLiteral("about") != null) {
				artist.setAbout(sol.getLiteral("about").toString());
			}
		}

		qe.close();
		return artist;
	}

	/**
	 * Create a new album to store the tracks available for the artist
	 * 
	 * @param albumURL
	 *            URL of the album created for storing properties
	 */
	private Album createAlbum(Resource albumURL) {
		Album album = new Album();

		// Query performed to store each individual track
		File albumTracksQueryFile = envMapping.getAlbumTracksQueryFile();
		String queryString = null;
		try {
			queryString = new Scanner(albumTracksQueryFile).useDelimiter("\\Z")
					.next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		ParameterizedSparqlString queryStr = new ParameterizedSparqlString(
				queryString);
		queryStr.setIri("album", albumURL.toString());

		Query query = QueryFactory.create(queryStr.toString());
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet results = qe.execSelect();

		// Store each individual track and its title
		while (results.hasNext()) {
			QuerySolution sol = results.next();
			if (sol.getLiteral("title") != null) {
				album.setTitle(sol.getLiteral("title").toString());
			}
			if (sol.getLiteral("track") != null) {
				album.addTrack(sol.getLiteral("track").toString());
			}
		}

		qe.close();
		return album;
	}
}
