/**
 * @author Raluca Lehadus (aca10ril@sheffield.ac.uk)
 */

package uk.ac.shef.dcs.com3504.query.wrapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Album implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8996192847752566209L;
	
	/**
	 * List of tracks in an album
	 */
	private List <String> tracks = new ArrayList<String>();
	
	/**
	 * Title of an album
	 */
	private String title;
	
	/**
	 * Set the value for attribute title
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Add a track to the list of songs
	 * 
	 * @param track
	 */
	public void addTrack(String track) {
		tracks.add(track);
	}
}
