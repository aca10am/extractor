/**
 * @author Mihai Popa-Matei (aca10map@sheffield.ac.uk)
 */


package uk.ac.shef.dcs.com3504.query.wrapper;

import java.io.Serializable;

public class MusicVenue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6076242957826340829L;

	/**
	 * Name of the music venue
	 */
	private String name;
	
	/**
	 * 
	 */
	private String image;
	
	/**
	 * Description of the music venue
	 */
	private String description;
	
	/**
	 * URL address to the wikipedia page for the music venue
	 */
	private String wikiPage;
	
	/**
	 * Type of object
	 */
	private String type = "MusicVenue";
	
	/**
	 * Geo-location for longitude of music venue
	 */
	private String geoLon;
	
	/**
	 * Geo-location for latitude of music venue
	 */
	private String geoLat;
	
	/**
	 * Street address of music venue
	 */
	private String street;
	
	/**
	 * City where the music venue is
	 */
	private String city;
	
	/**
	 * Country where the music venue is
	 */
	private String country;
	
	/**
	 * Zip code of the music venue
	 */
	private String zip;
	
	/**
	 * Counter to tell if information was found or not
	 */
	private boolean found;

	/**
	 * Set the value for attribute found
	 * 
	 * @param found
	 */
	public void setFound(boolean found) {
		this.found = found;
	}

	/**
	 * Set the value for attribute street
	 * 
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Set the value for attribute city
	 * 
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Set the value for attribute country
	 * 
	 * @param country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Set the value for attribute zip
	 * 
	 * @param zip
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * Set the value for attribute geoLon
	 * 
	 * @param geoLon
	 */
	public void setGeoLon(String geoLon) {
		this.geoLon = geoLon;
	}

	/** 
	 * Set the value for attribute geoLat
	 * 
	 * @param geoLat
	 */
	public void setGeoLat(String geoLat) {
		this.geoLat = geoLat;
	}

	/**
	 * Constructor function; creates a new Music Venue with a given name
	 * 
	 * @param name
	 */
	public MusicVenue(String name) {
		this.name = name;
	}

	/**
	 * Set the value for attribute name
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Set the value for attribute image
	 * 
	 * @param image
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Set the value for attribute description
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Set the value for attribute wikiPage
	 * 
	 * @param wikiPage
	 */
	public void setWikiPage(String wikiPage) {
		this.wikiPage = wikiPage;
	}
}
