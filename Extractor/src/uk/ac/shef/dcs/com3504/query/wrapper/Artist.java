/**
 * @author Mihai Popa-Matei (aca10map@sheffield.ac.uk)
 */


package uk.ac.shef.dcs.com3504.query.wrapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Artist implements Serializable {

	private static final long serialVersionUID = 8979808592769171100L;

	/**
	 * Value for storing the image
	 */
	private String image;
	
	/**
	 * Biography of artist
	 */
	private String biography;
	
	/**
	 * List of albums made by the artist
	 */
	private List<Album> albums = new ArrayList<Album>();
	
	/**
	 * URL to the wikipedia page of the artist
	 */
	private String wikiPage;
	
	/**
	 * Number of likes on Facebook
	 */
	private int likes;
	
	/**
	 * Short description of artist
	 */
	private String about;
	
	/**
	 * Name of artist
	 */
	private String name;
	
	/**
	 * Type of object
	 */
	private String type = "Artist";
	
	/**
	 * Counter to tell if information was found or not
	 */
	private boolean found;
	
	/**
	 * Set the value for attribute found
	 * 
	 * @param found
	 */
	public void setFound(boolean found) {
		this.found = found;
	}

	/**
	 * Constructor function; create new artist with a given name
	 * 
	 * @param name
	 */
	public Artist(String name) {
		this.name = name;
	}

	/**
	 * Set the value for attribute artistName
	 * 
	 * @param artistName
	 */
	public void setTitle(String artistName) {
		this.name = artistName;
	}

	/**
	 * Set the value for attribute image
	 * 
	 * @param image
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Set the value for attribute biography
	 * 
	 * @param biography
	 */
	public void setBiography(String biography) {
		this.biography = biography;
	}

	/**
	 * Set the value for attribute album
	 * 
	 * @param album
	 */
	public void addAlbum(Album album) {
		albums.add(album);
	}

	/**
	 * Set the value for attribute wikiPage
	 * 
	 * @param wikiPage
	 */
	public void setWikiPage(String wikiPage) {
		this.wikiPage = wikiPage;
	}

	/**
	 * Set the value for attribute likes
	 * 
	 * @param likes
	 */
	public void setLikes(int likes) {
		this.likes = likes;
	}

	/**
	 * Set the value for attribute about
	 * 
	 * @param about
	 */
	public void setAbout(String about) {
		this.about = about;
	}
}
