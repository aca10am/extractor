/**
 * @author Mihai Popa-Matei (aca10map@sheffield.ac.uk)
 */


package uk.ac.shef.dcs.com3504.query;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.google.gson.Gson;

import uk.ac.shef.dcs.com3504.mapping.EnvMapping;
import uk.ac.shef.dcs.com3504.query.wrapper.Artist;
import uk.ac.shef.dcs.com3504.query.wrapper.MusicVenue;

public class QueryController {

	private String queryVenue;
	private String queryArtist;
	private InputStream rdfFile;
	private EnvMapping envMapping;
	private String jsonResult;
	
	/**
	 * Function to return the json result obtained
	 */
	public String getJsonResult() {
		return jsonResult;
	}

	/**
	 * Constructor used to store the attribute regarding the artists or venues queried
	 * 
	 * @param venue
	 *            Attribute describing the venue inputed
	 * @param artist
	 *            Attribute describing the artist inputed   
	 * @param rdf
	 *            Input the rdf file to be read           
	 */
	public QueryController(String venue, String artist, String rdf) {
		queryVenue = venue;
		queryArtist = artist;
		envMapping = new EnvMapping();
		rdfFile = stringToInputStream(rdf);
	}

	/**
	 * Function to query if the information needed is about artists or music venues
	 */
	public void execute() {
		
		// check if information is about artists
		if (queryVenue.isEmpty()) {
			// process available information regarding the specific artist
			ArtistQuery aq = new ArtistQuery(queryArtist, rdfFile, envMapping);
			Artist artist = aq.execute();
			
			Gson gson = new Gson();
			jsonResult = gson.toJson(artist);
		} else {
			// process available information regarding the specific music venue
			MusicVenueQuery mvq = new MusicVenueQuery(queryVenue, rdfFile, envMapping);
			MusicVenue musicVenue = mvq.execute();
			
			Gson gson = new Gson();
			jsonResult = gson.toJson(musicVenue);
		}
		
	}
	
	/**
	 * Function to return a string converted into byteArray string
	 * 
	 * @param str
	 *            String to be converted
	 */
	private InputStream stringToInputStream(String str) {
		return new ByteArrayInputStream(str.getBytes());
	}
}
