$(function() {
	// Validate the current field each time the user types
	$('input').keyup(function() {
		// Retrieve the field's name, get the correct regex based on the
		// field's name, then remove any previous error, and validate the field
		field = $(this).attr("name");
		regex = (field == "venue") ? /[^a-zA-Z0-9_ ]/ : /[^a-zA-Z0-9 ]/;
		removeFieldErrors(field);
		validateField(field, regex);
	});

	// Validate the form on every submission and submit the form if the data is
	// valid
	$('form.ajax').submit(function() {
		
		if (validate($("#venue").val(), $("#artist").val())) {
			$("#artistResult").addClass("hidden");
			$("#musicVenueResult").addClass("hidden");
			$("#results").addClass("hidden");
			$("#no-results").addClass("hidden");
			
			// After the form is submitted show a grey overlay with the message
			// "Please wait", so that the user knows that the query is
			// processed, and the user is prevented from typing anything in the
			// form.
			$.blockUI();
			$.post(this.action, $(this).serialize(), function(data) {
				console.log(JSON.parse(data));
				result = JSON.parse(data);
				
				if (result.found && result.type == "Artist") {
					renderArtist(result);
					$("#artistResult").removeClass("hidden");
					$("#results").removeClass("hidden");
					
					// Move the scroll to the results section
					$('html, body').animate({
						scrollTop : $("#results").offset().top
					}, 2000);
				} else if (result.found && result.type == "MusicVenue") {
					renderMusicVenue(result);
					$("#musicVenueResult").removeClass("hidden");
					$("#results").removeClass("hidden");
					
					// Move the scroll to the results section
					$('html, body').animate({
						scrollTop : $("#results").offset().top
					}, 2000);
				} else {
					$("#no-results").removeClass("hidden");
				}
				// Remove the grey overlay, so that the user is able to see
				// results, and to complete the form again.
				$.unblockUI();
				$('#venue').val("");
				$('#artist').val("");
			});
		}
		return false;
	});
});

function renderMusicVenue(musicVenue) {
	$("#musicVenueName").text(musicVenue.name);
	$("#musicVenueImage").attr("src", musicVenue.image);
	$("#musicVenueWiki").attr("href", musicVenue.wikiPage);
	$("#musicVenueWiki").text(musicVenue.wikiPage);
	$("#musicVenueDescription").text(musicVenue.description);
	$("#musicVenueCountry").text(musicVenue.country);
	$("#musicVenueCity").text(musicVenue.city);
	$("#musicVenueStreet").text(musicVenue.street);
	$("#musicVenueZip").text(musicVenue.zip);
	$("#musicVenueGeoLon").text(musicVenue.geoLon);
	$("#musicVenueGeoLat").text(musicVenue.geoLat);
}

function renderArtist(artist) {
	$("#artistName").text(artist.name);
	$("#artistImage").attr("src", artist.image);
	$("#artistWiki").attr("href", artist.wikiPage);
	$("#artistWiki").text(artist.wikiPage);
	$("#artistAbout").text(artist.about);
	$("#artistBiography").text(artist.biography);
	$("#artistLikes").text(artist.likes);
	$("#artistAlbums").html(generateAlbumsHTML(artist.albums));
}

function generateAlbumsHTML(albums) {
	var albumsLength = albums.length;
	var albumsHTML = "<ul>";
	
	for (var i = 0; i < albumsLength; i++) {
		var album = albums[i];
		var tracksLength = album.tracks.length;
		
		if (tracksLength > 0) {
			albumsHTML += "<li>" + album.title + "</li>";
			
			albumsHTML += "<ul>";
			for (var j = 0; j < tracksLength; j++) {
				albumsHTML += "<li>" + album.tracks[j] + "</li>";
			}
			albumsHTML += "</ul>";
		}
	}
	
	albumsHTML += "</ul>";
	return albumsHTML;
}

/**
 * Validate the a field from the form. If the field does not match the given
 * regular expression, then add a validation message, and add the error class to
 * the group
 * 
 * @param field
 * @param regex
 * @returns true if the field is valid, otherwise false
 */
function validateField(field, regex) {
	$field = $("#" + field);

	// Check if the venue only contains alphanumeric items
	if (regex.test($field.val())) {
		$field.parent().parent().addClass("error");
		$field.siblings(".help-inline").text(
				"The " + field + " can only be alphanumeric");
		return false;
	}
	return true;
}

/**
 * Validate a given venue and artist. If they are not valid, add validation
 * messages to the fields
 * 
 * @param venue
 * @param artist
 * @returns true if both fields are valid, otherwise false
 */
function validate(venue, artist) {
	// Remove the previously added errors
	removeErrors();

	// Check if at least one of them is not empty
	if (venue == "" && artist == "") {
		$("#venuesGroup").addClass("error");
		$("#artistGroup").addClass("error");
		$("#venueHelp").text(
				"At least one of the venue or artist must be present");
		return false;
	}
	if (!validateField("venue", /[^a-zA-Z0-9_ ]/)
			|| !validateField("artist", /[^a-zA-Z0-9 ]/)) {
		return false;
	}
	return true;
}

/**
 * Remove all the error messages, and the error class for a given form of the
 * field
 * 
 * @param field
 */
function removeFieldErrors(field) {
	$field = $("#" + field);
	$field.parent().parent().removeClass("error");
	$field.siblings(".help-inline").text("");
}

/**
 * Remove all the error messages, and the error classes for all fields of the
 * form
 */
function removeErrors() {
	$("#venueGroup").removeClass("error");
	$("#artistGroup").removeClass("error");
	$(".help-inline").text("");
}